// Khai báo thư viên mongooseJS
const mongoose = require("mongoose");

// Khai báo Class Schema
const Schema = mongoose.Schema;

// Khởi tạo 1 instance courseSchema từ class Schema
const courseSchema = new Schema({
    courseCode: {
        type: String,
        required: true,
        unique: true
    },
    courseName: {
        type: String,
        required: true
    },
    price: {
        type: Number,
        required:true
    },
    discountPrice: {
        type:Number,
        requiredL:true
    },
    duration: {
        type:String,
        requiredL:true
    },
    level: {
        type:String,
        requiredL:true
    },
    coverImage: {
        type:String,
        requiredL:true
    },
    teacherName: {
        type:String,
        requiredL:true
    },
    teacherPhoto: {
        type:String,
        required:true
    },
    isPopular: {
        type:Boolean,
        requiredL:true
    },
    isTrending: {
        type:Boolean,
        requiredL:false
    },
    // Một course có nhiều review
    reviews: [{
        type: mongoose.Types.ObjectId,
        ref: "Review"
    }]
    // Trường hợp 1 course có 1 reivew 
    // reviews: {
    //     type: mongoose.Types.ObjectId,
    //     ref: "Review"
    // }
}, {
    timestamps: true
})

module.exports = mongoose.model("Courses", courseSchema);