const express = require('express');
const router = express.Router();
const path = require('path');
// Import middleware
const signInMiddleware = require('../middlewares/signInMiddleware')



router.get("/", signInMiddleware.getDateAndUrlMiddleware, (req, res) => {
    res.sendFile(path.join(process.cwd() + "/app/views/pizza365index.html" ))
})

router.use(express.static(process.cwd() + '/app/views'))

module.exports = router;
