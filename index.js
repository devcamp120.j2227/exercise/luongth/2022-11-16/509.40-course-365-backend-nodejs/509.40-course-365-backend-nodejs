// Khai báo thư viên Express
const express = require("express");
// Khai báo thư viện Mongoose
const mongoose = require("mongoose");

// Khởi tạo app express
const app = express();

// Khai báo cổng chạy app 
const port = 8000;

// Khai báo router app
const courseRouter = require("./app/routes/courseRouter");
const reviewRouter = require("./app/routes/reviewRouter");
const viewRouter = require("./app/routes/signInRouter");



// Cấu hình request đọc được body json
app.use(express.json());

app.use((request, response, next) => {
    console.log("Current time: ", new Date());
    next();
})

app.use((request, response, next) => {
    console.log("Request method: ", request.method);
    next();
})

mongoose.connect("mongodb://127.0.0.1:27017/test", (error) => {
    if(error) throw error;
    console.log("Connect MongoDB successfully!");
})

// Khai báo API /
app.get("/", (request, response) => {
    console.log("Call API GET /");

    response.json({
        message: "Devcamp Middleware Express APP"
    })
})

// App sử dụng router
app.use("/api", courseRouter);
app.use("/api", reviewRouter);
app.use("/api", viewRouter);


// Chạy app trên cổng
app.listen(port, () => {
    console.log("App listening on port:", port);
})